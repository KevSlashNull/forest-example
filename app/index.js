const { forest } = require('forest-framework');
const IndexController = require('./Controllers/IndexController');

forest.run(__dirname, true);

forest.get('/', IndexController, 'index').middleware('request_log');
forest.get('/admin', IndexController, 'admin').middleware('admin');
