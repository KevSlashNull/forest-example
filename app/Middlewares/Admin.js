const { Middleware, ForestRequest } = require('forest-framework');

class Admin extends Middleware {
  /**
   * @param {ForestRequest} request
   * @param {function} next
   */
  async handle(request, next) {
    if (!request.query.has('admin')) {
      return { error: 'forbidden' };
    }
    return next();
  }
}

module.exports = Admin;
