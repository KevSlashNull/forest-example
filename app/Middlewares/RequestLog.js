const { Middleware, ForestRequest, forest } = require('forest-framework');

class RequestLog extends Middleware {
  /** @param {ForestRequest} request */
  async handle(request, next) {
    const now = new Date();

    console.log(`[${now.toLocaleTimeString()}] ${request.ip}: ${request.method.toUpperCase()} ${request.url.pathname}`);

    return next();
  }
}

module.exports = RequestLog;
