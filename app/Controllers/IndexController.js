const { Controller } = require('forest-framework');

class IndexController extends Controller {
  async index() {
    return { dog: '🐶' };
  }

  async admin() {
    return { doggos: ['🐕', '🐩', '🐕‍🦺', '🦮'] };
  }
}

module.exports = IndexController;
